# base docker image with python3, pip and libs needed for the script
FROM ubuntu:18.04
RUN apt-get update \
  && apt-get install -y python3-pip python3-dev \
  && cd /usr/local/bin \
  && ln -s /usr/bin/python3 python \
  && pip3 install --upgrade pip \
  && pip3 install statsd \ 
  && pip3 install filelock \ 
  && pip3 install psutil
COPY memory_stats/memory_stats.py .
CMD ["/usr/local/bin/python", "memory_stats.py"]