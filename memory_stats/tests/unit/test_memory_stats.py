import unittest
import memory_stats

class MemoryStats(unittest.TestCase):

    def test_proccess_list_name(self):
        process_list = memory_stats.getProcessInfo()
        for p in process_list:
            self.assertTrue(p['name'])

    def test_proccess_list_memory(self):
        process_list = memory_stats.getProcessInfo()
        for p in process_list:
            self.assertTrue(p['vms'])

if __name__ == '__main__':
    unittest.main()