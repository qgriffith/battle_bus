import psutil
import os
import re
import statsd
import time
import socket
import sys
import argparse
from filelock import Timeout, FileLock

parser = argparse.ArgumentParser(description="Sends Memory stats per process to statsd server.")
parser.add_argument('statsd_host', type=str, help="hostname of the statsd server")
args = parser.parse_args()
statsd_host = args.statsd_host

PREFIX='%s.' % socket.gethostname()
SENDINTERVAL = 10

def getProcessInfo():
  listOfProcObjects = []
  # Iterate over all running processes
  for proc in psutil.process_iter():
    # Get process detail as dictionary
    try:
      pinfo = proc.as_dict(attrs=['name'])
      pinfo['vms'] = proc.memory_info().vms
      # Append dict of process detail in list
      listOfProcObjects.append(pinfo)
      listOfProcObjects = sorted(listOfProcObjects, key=lambda procObj: procObj['vms'], reverse=True)
    except:
      print("Can't get info for a process, skipping...")
  return listOfProcObjects

def send_process_memory_stats():
  while True:
    try:
      listofProcesses = getProcessInfo()
      for p in listofProcesses:
        process_name = p['name']

        if p['vms']:
          try:
            c = statsd.StatsClient(statsd_host, 8125, prefix=PREFIX + 'system.processes')          
            c.gauge(f"{process_name}.memory.vms", p['vms'], rate=1, delta=False)
            time.sleep(SENDINTERVAL)
          except:
            print("Unable to connect to statsd server",  sys.exc_info()[0])
            return
        
    except:
      print("Unexpected error:", sys.exc_info()[0])
      time.sleep(SENDINTERVAL * 2)

lock = FileLock("mem_stat.lock", timeout=120)

try:
  lock.acquire(timeout=120)
  send_process_memory_stats()
except Timeout:
    print("Another instance of this application currently holds the lock.")
finally:
  lock.release()
  
