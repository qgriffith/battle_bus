import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')

def test_buildessential_is_installed(host):
    buildessential = host.package("build-essential")
    assert buildessential.is_installed

def test_gcc_is_installed(host):
    gcc = host.package("gcc")
    assert gcc.is_installed

def test_python3_is_installed(host):
    python3 = host.package("python3-dev")
    assert python3.is_installed

def test_pip3_is_installed(host):
    pip3 = host.package("python3-pip")
    assert pip3.is_installed

def test_memstat_dir(host):
    memstat_dir = host.file('/opt/memory_stats/')
    assert memstat_dir.exists

def test_memstat_script(host):
    memstat_script = host.file('/opt/memory_stats/memory_stats.py')
    assert memstat_script.user == 'root'
    assert memstat_script.group == 'root'
    assert oct(memstat_script.mode) == '0o744'
    assert memstat_script.exists