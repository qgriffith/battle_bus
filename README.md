# Battle Bus
Project Battle Bus is a python3 script that is delivered and ran via Ansible to gather per process memory and sent it to a statd server

# Demo
Using docker-compose a demo can be ran in that a docker image running the script will send the stats to another image that is running statd. The statd web UI port is exposed on localhost:8000
```docker-compose up --build```

# Running locally
You can run the script locally as long as python3 and the modules statsd and psutils is installed
```memstat.py <statdhost>```

# Running Unit Test
You can run unit test by going into the memory_stats folder and running
```python -m unitest tests/unit/test_memory_stats.py```

# Running across many servers at once
Using Ansible the role can target many servers. Ansible will lay down the script and launch the script in the background. The memory_stats.py script will create a lock file while running to prevent it from running several times. Update ansible/hosts with the list of host to run it on
```ansible-playbook -f 50 -i hosts battle_bus.yml```